package top.hmtools.wxmp.core.eventModels;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import top.hmtools.wxmp.core.annotation.WxmpMessage;
import top.hmtools.wxmp.core.model.message.BaseMessage;
import top.hmtools.wxmp.core.model.message.enums.MsgType;

/**
 * 文本消息
 * @author Hybomyth
 *
 */
@WxmpMessage(msgType=MsgType.text)
public class TextMessage extends BaseMessage{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8090390109332033707L;
	
	/**
	 * 文本消息内容
	 */
	@XStreamAlias("Content")
	private String content;

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	@Override
	public void configXStream(XStream xStream) {
		
	}
	
	

}
