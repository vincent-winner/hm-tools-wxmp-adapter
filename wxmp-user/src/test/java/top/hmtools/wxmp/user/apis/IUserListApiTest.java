package top.hmtools.wxmp.user.apis;

import org.junit.Test;

import top.hmtools.wxmp.user.BaseTest;
import top.hmtools.wxmp.user.model.UserListParam;
import top.hmtools.wxmp.user.model.UserListResult;

public class IUserListApiTest extends BaseTest {
	
	private IUserListApi iUserListApi ;
	

	@Test
	public void testGetUserList() {
		UserListParam userListParam = new UserListParam();
		UserListResult userList = this.iUserListApi.getUserList(userListParam);
		this.printFormatedJson("用户管理--获取用户列表", userList);
	}


	@Override
	public void initSub() {
		this.iUserListApi = this.wxmpSession.getMapper(IUserListApi.class);
	}

}
