package top.hmtools.wxmp.message.template.apis;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import top.hmtools.wxmp.core.model.ErrcodeBean;
import top.hmtools.wxmp.message.BaseTest;
import top.hmtools.wxmp.message.template.model.DataItem;
import top.hmtools.wxmp.message.template.model.DelTemplateParam;
import top.hmtools.wxmp.message.template.model.GetIndustryResult;
import top.hmtools.wxmp.message.template.model.SendTemplateMessageParam;
import top.hmtools.wxmp.message.template.model.SendTemplateMessageResult;
import top.hmtools.wxmp.message.template.model.SetIndustryParam;
import top.hmtools.wxmp.message.template.model.TemplateIdParam;
import top.hmtools.wxmp.message.template.model.TemplateIdResult;
import top.hmtools.wxmp.message.template.model.TemplateListResult;

public class ITemplateApiTest extends BaseTest{
	
	private ITemplateApi templateApi;

	@Test
	public void testSetIndustry() {
		SetIndustryParam setIndustryParam = new SetIndustryParam();
		setIndustryParam.setIndustry_id1("2");
		setIndustryParam.setIndustry_id2("3");
		ErrcodeBean result = this.templateApi.setIndustry(setIndustryParam );
		this.printFormatedJson("1 设置所属行业", result);
	}

	@Test
	public void testGetIndustry() {
		GetIndustryResult result = this.templateApi.getIndustry();
		this.printFormatedJson("2 获取设置的行业信息", result);
	}

	/**
	 * 因测试账号原因，暂时未测试通过
	 */
	@Test
	public void testGetTemplateId() {
		TemplateIdParam templateIdParam = new TemplateIdParam();
		templateIdParam.setTemplate_id_short("1");
		TemplateIdResult result = this.templateApi.getTemplateId(templateIdParam );
		this.printFormatedJson("3 获得模板ID", result);
	}

	@Test
	public void testGetAllPrivateTemplate() {
		TemplateListResult result = this.templateApi.getAllPrivateTemplate();
		this.printFormatedJson("4 获取模板列表", result);
	}

	@Test
	public void testDelPrivateTemplate() {
		DelTemplateParam delTemplateParam = new DelTemplateParam();
		delTemplateParam.setTemplate_id("jqE5LnWMmfhaB17h5_lIB67v1YNKhE8BHypziYND2yE");
		ErrcodeBean result = this.templateApi.delPrivateTemplate(delTemplateParam );
		this.printFormatedJson("5 删除模板", result);
	}

	@Test
	public void testSendTemplateMessage() {
		SendTemplateMessageParam sendTemplateMessageParam = new SendTemplateMessageParam();
		Map<String, DataItem> data = new HashMap<>();
		data.put("aaa", new DataItem("hahahaha","#ffffff"));
		sendTemplateMessageParam.setData(data );
		sendTemplateMessageParam.setTemplate_id("NS6GJPc6BR86dXpia2jwZDXzACOi3LqUUazUgmbT4jE");
		sendTemplateMessageParam.setTouser("o-OZ0v8HcoPJiNlZLTOZYrkeZUG0");
		SendTemplateMessageResult result = this.templateApi.sendTemplateMessage(sendTemplateMessageParam );
		this.printFormatedJson("6 发送模板消息", result);
	}

	@Override
	public void initSub() {
		this.templateApi = this.wxmpSession.getMapper(ITemplateApi.class);
	}

}
