package top.hmtools.wxmp.message;

import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import top.hmtools.wxmp.AppId;
import top.hmtools.wxmp.core.WxmpSession;
import top.hmtools.wxmp.core.WxmpSessionFactory;
import top.hmtools.wxmp.core.WxmpSessionFactoryBuilder;
import top.hmtools.wxmp.core.access_handle.DefaultAccessTokenHandle;
import top.hmtools.wxmp.core.configuration.AppIdSecretBox;
import top.hmtools.wxmp.core.configuration.AppIdSecretPair;
import top.hmtools.wxmp.core.configuration.WxmpConfiguration;

public abstract class BaseTest {
	
	protected Logger logger = LoggerFactory.getLogger(this.getClass());
	
	/**
	 * 微信公众号接口链接会话对象实例
	 */
	protected WxmpSession wxmpSession;
	
	/**
	 * 微信公众号接口链接会话工厂对象实例
	 */
	protected WxmpSessionFactory factory;

	private ObjectMapper objectMapper;

	@Before
	public void init(){
		//TODO 必要时，可以对 httpclient进行设置
//		HmHttpClientFactoryHandle.setConnectionKeepAliveStrategy(connectionKeepAliveStrategy);
		//…………
		
		/**
		 * 微信公众号接口操作全局配置类对象实例
		 */
		WxmpConfiguration configuration = new WxmpConfiguration();
		
		//设置 存储appid，appsecret 数据对 的盒子
		AppIdSecretBox appIdSecretBox = new AppIdSecretBox() {
			
			@Override
			public AppIdSecretPair getAppIdSecretPair() {
				AppIdSecretPair appIdSecretPair = new AppIdSecretPair();
				appIdSecretPair.setAppid(AppId.appid);
				appIdSecretPair.setAppsecret(AppId.appsecret);
				return appIdSecretPair;
			}
		};
		
		//设置 获取access token 的中间件
		DefaultAccessTokenHandle accessTokenHandle = new DefaultAccessTokenHandle(appIdSecretBox);
		configuration.setAccessTokenHandle(accessTokenHandle);
		
		this.factory = WxmpSessionFactoryBuilder.build(configuration);
		this.wxmpSession = this.factory.openSession();
		
		this.initSub();
	}
	
	
	/**
	 * 初始化子类
	 */
	public abstract void initSub();
	
	
	
	
	
	
	
	/**
	 * 格式化打印json字符串到控制台
	 * @param title
	 * @param obj
	 */
	protected synchronized void printFormatedJson(String title,Object obj) {
		if(this.objectMapper == null){
			this.objectMapper = new ObjectMapper();
		}
		try {
			//阿里的fastjson具有很好的兼容性，所以才多次一举
			String jsonString = JSON.toJSONString(obj);
			Object tempObj = JSON.parse(jsonString);
			String formatedJsonStr = this.objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(tempObj);
			this.logger.info("\n{}：\n{}",title,formatedJsonStr);
		} catch (JsonProcessingException e) {
			this.logger.error("格式化打印json异常：",e);
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
