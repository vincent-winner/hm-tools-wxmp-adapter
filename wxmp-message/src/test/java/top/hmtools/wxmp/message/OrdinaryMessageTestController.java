package top.hmtools.wxmp.message;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import top.hmtools.wxmp.core.annotation.WxmpController;
import top.hmtools.wxmp.core.annotation.WxmpRequestMapping;
import top.hmtools.wxmp.message.ordinary.model.ImageMessage;
import top.hmtools.wxmp.message.ordinary.model.LinkMessage;
import top.hmtools.wxmp.message.ordinary.model.LocationMessage;
import top.hmtools.wxmp.message.ordinary.model.ShortvideoMessage;
import top.hmtools.wxmp.message.ordinary.model.TextMessage;
import top.hmtools.wxmp.message.ordinary.model.VideoMessage;
import top.hmtools.wxmp.message.ordinary.model.VoiceMessage;

/**
 * 消息管理 -- 接收普通消息 
 * @author HyboWork
 *
 */
@WxmpController
public class OrdinaryMessageTestController {

	final Logger logger = LoggerFactory.getLogger(OrdinaryMessageTestController.class);
	private ObjectMapper objectMapper;
	
	/**
	 * 文本消息
	 * @param msg
	 */
	@WxmpRequestMapping
	public void executeMessage(TextMessage msg){
		this.printFormatedJson("文本消息", msg);
	}
	
	
	@WxmpRequestMapping
	public void executeMessage(ImageMessage msg){
		this.printFormatedJson("图片消息", msg);
	}
	
	
	@WxmpRequestMapping
	public void executeMessage(VoiceMessage msg){
		this.printFormatedJson("语音消息", msg);
	}
	
	
	@WxmpRequestMapping
	public void executeMessage(VideoMessage msg){
		this.printFormatedJson("视频消息", msg);
	}
	
	
	@WxmpRequestMapping
	public void executeMessage(ShortvideoMessage msg){
		this.printFormatedJson("小视频消息", msg);
	}
	
	
	@WxmpRequestMapping
	public void executeMessage(LocationMessage msg){
		this.printFormatedJson("地理位置消息", msg);
	}
	
	
	@WxmpRequestMapping
	public void executeMessage(LinkMessage msg){
		this.printFormatedJson("链接消息", msg);
	}
	
	
	
	
	
	
	/**
	 * 格式化打印json字符串到控制台
	 * @param title
	 * @param obj
	 */
	protected synchronized void printFormatedJson(String title,Object obj) {
		if(this.objectMapper == null){
			this.objectMapper = new ObjectMapper();
		}
		try {
			//阿里的fastjson具有很好的兼容性，所以才多次一举
			String jsonString = JSON.toJSONString(obj);
			Object tempObj = JSON.parse(jsonString);
			String formatedJsonStr = this.objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(tempObj);
			this.logger.info("\n{}：\n{}",title,formatedJsonStr);
		} catch (JsonProcessingException e) {
			this.logger.error("格式化打印json异常：",e);
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
