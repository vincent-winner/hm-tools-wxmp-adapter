package top.hmtools.wxmp.message;

import org.junit.Test;

import top.hmtools.wxmp.core.DefaultWxmpMessageHandle;

/**
 * 模拟测试接收微信公众号自定义菜单相关事件
 * 
 * @author HyboWork
 *
 */
public class WxmpMessageTest {

	/**
	 * 测试 接收普通消息
	 */
	@Test
	public void executeOrdinaryMessageTest() {
		// 实例化消息处理handle
		DefaultWxmpMessageHandle defaultWxmpMessageHandle = new DefaultWxmpMessageHandle();

		// 实例化实际处理指定消息的controller，并加入handle映射
		OrdinaryMessageTestController menuMessageTestController = new OrdinaryMessageTestController();
		defaultWxmpMessageHandle.addMessageMetaInfo(menuMessageTestController);

		//
		String xml = "<xml>  <ToUserName><![CDATA[toUser]]></ToUserName>  <FromUserName><![CDATA[fromUser]]></FromUserName>  <CreateTime>1348831860</CreateTime>  <MsgType><![CDATA[text]]></MsgType>  <Content><![CDATA[this is a test]]></Content>  <MsgId>1234567890123456</MsgId></xml>";
		defaultWxmpMessageHandle.processXmlData(xml);

		//
		xml = "<xml>  <ToUserName><![CDATA[toUser]]></ToUserName>  <FromUserName><![CDATA[fromUser]]></FromUserName>  <CreateTime>1348831860</CreateTime>  <MsgType><![CDATA[image]]></MsgType>  <PicUrl><![CDATA[this is a url]]></PicUrl>  <MediaId><![CDATA[media_id]]></MediaId>  <MsgId>1234567890123456</MsgId></xml>";
		defaultWxmpMessageHandle.processXmlData(xml);

		//
		xml = "<xml>  <ToUserName><![CDATA[toUser]]></ToUserName>  <FromUserName><![CDATA[fromUser]]></FromUserName>  <CreateTime>1357290913</CreateTime>  <MsgType><![CDATA[voice]]></MsgType>  <MediaId><![CDATA[media_id]]></MediaId>  <Format><![CDATA[Format]]></Format>  <MsgId>1234567890123456</MsgId></xml>";
		defaultWxmpMessageHandle.processXmlData(xml);

		//
		xml = "<xml>  <ToUserName><![CDATA[toUser]]></ToUserName>  <FromUserName><![CDATA[fromUser]]></FromUserName>  <CreateTime>1357290913</CreateTime>  <MsgType><![CDATA[voice]]></MsgType>  <MediaId><![CDATA[media_id]]></MediaId>  <Format><![CDATA[Format]]></Format>  <Recognition><![CDATA[腾讯微信团队]]></Recognition>  <MsgId>1234567890123456</MsgId></xml>";
		defaultWxmpMessageHandle.processXmlData(xml);

		//
		xml = "<xml>  <ToUserName><![CDATA[toUser]]></ToUserName>  <FromUserName><![CDATA[fromUser]]></FromUserName>  <CreateTime>1357290913</CreateTime>  <MsgType><![CDATA[video]]></MsgType>  <MediaId><![CDATA[media_id]]></MediaId>  <ThumbMediaId><![CDATA[thumb_media_id]]></ThumbMediaId>  <MsgId>1234567890123456</MsgId></xml>";
		defaultWxmpMessageHandle.processXmlData(xml);

		//
		xml = "<xml>  <ToUserName><![CDATA[toUser]]></ToUserName>  <FromUserName><![CDATA[fromUser]]></FromUserName>  <CreateTime>1357290913</CreateTime>  <MsgType><![CDATA[shortvideo]]></MsgType>  <MediaId><![CDATA[media_id]]></MediaId>  <ThumbMediaId><![CDATA[thumb_media_id]]></ThumbMediaId>  <MsgId>1234567890123456</MsgId></xml>";
		defaultWxmpMessageHandle.processXmlData(xml);

		//
		xml = "<xml>  <ToUserName><![CDATA[toUser]]></ToUserName>  <FromUserName><![CDATA[fromUser]]></FromUserName>  <CreateTime>1351776360</CreateTime>  <MsgType><![CDATA[location]]></MsgType>  <Location_X>23.134521</Location_X>  <Location_Y>113.358803</Location_Y>  <Scale>20</Scale>  <Label><![CDATA[位置信息]]></Label>  <MsgId>1234567890123456</MsgId></xml>";
		defaultWxmpMessageHandle.processXmlData(xml);

		//
		xml = "<xml>  <ToUserName><![CDATA[toUser]]></ToUserName>  <FromUserName><![CDATA[fromUser]]></FromUserName>  <CreateTime>1351776360</CreateTime>  <MsgType><![CDATA[link]]></MsgType>  <Title><![CDATA[公众平台官网链接]]></Title>  <Description><![CDATA[公众平台官网链接]]></Description>  <Url><![CDATA[url]]></Url>  <MsgId>1234567890123456</MsgId></xml>";
		defaultWxmpMessageHandle.processXmlData(xml);

	}

	/**
	 * 接收事件推送 测试
	 */
	@Test
	public void executeEventPushMessageTest() {
		// 实例化消息处理handle
		DefaultWxmpMessageHandle defaultWxmpMessageHandle = new DefaultWxmpMessageHandle();

		// 实例化实际处理指定消息的controller，并加入handle映射
		EventPushMessageTestController messageTestController = new EventPushMessageTestController();
		defaultWxmpMessageHandle.addMessageMetaInfo(messageTestController);

		//
		String xml = "<xml>  <ToUserName><![CDATA[toUser]]></ToUserName>  <FromUserName><![CDATA[FromUser]]></FromUserName>  <CreateTime>123456789</CreateTime>  <MsgType><![CDATA[event]]></MsgType>  <Event><![CDATA[subscribe]]></Event></xml>";
		defaultWxmpMessageHandle.processXmlData(xml);

		//
		xml = "<xml>  <ToUserName><![CDATA[toUser]]></ToUserName>  <FromUserName><![CDATA[FromUser]]></FromUserName>  <CreateTime>123456789</CreateTime>  <MsgType><![CDATA[event]]></MsgType>  <Event><![CDATA[unsubscribe]]></Event></xml>";
		defaultWxmpMessageHandle.processXmlData(xml);

		//FIXME TODO 该事件消息 与 SubscribeEventMessage 同 event，区别在 eventKey 
		xml = "<xml>  <ToUserName><![CDATA[toUser]]></ToUserName>  <FromUserName><![CDATA[FromUser]]></FromUserName>  <CreateTime>123456789</CreateTime>  <MsgType><![CDATA[event]]></MsgType>  <Event><![CDATA[subscribe]]></Event>  <EventKey><![CDATA[qrscene_123123]]></EventKey>  <Ticket><![CDATA[TICKET]]></Ticket></xml>";
		defaultWxmpMessageHandle.processXmlData(xml);

		//
		xml = "<xml>  <ToUserName><![CDATA[toUser]]></ToUserName>  <FromUserName><![CDATA[FromUser]]></FromUserName>  <CreateTime>123456789</CreateTime>  <MsgType><![CDATA[event]]></MsgType>  <Event><![CDATA[SCAN]]></Event>  <EventKey><![CDATA[SCENE_VALUE]]></EventKey>  <Ticket><![CDATA[TICKET]]></Ticket></xml> ";
		defaultWxmpMessageHandle.processXmlData(xml);

		//
		xml = "<xml>  <ToUserName><![CDATA[toUser]]></ToUserName>  <FromUserName><![CDATA[fromUser]]></FromUserName>  <CreateTime>123456789</CreateTime>  <MsgType><![CDATA[event]]></MsgType>  <Event><![CDATA[LOCATION]]></Event>  <Latitude>23.137466</Latitude>  <Longitude>113.352425</Longitude>  <Precision>119.385040</Precision></xml>";
		defaultWxmpMessageHandle.processXmlData(xml);

		//
		xml = "<xml>  <ToUserName><![CDATA[toUser]]></ToUserName>  <FromUserName><![CDATA[FromUser]]></FromUserName>  <CreateTime>123456789</CreateTime>  <MsgType><![CDATA[event]]></MsgType>  <Event><![CDATA[CLICK]]></Event>  <EventKey><![CDATA[EVENTKEY]]></EventKey></xml>";
		defaultWxmpMessageHandle.processXmlData(xml);

		//
		xml = "<xml>  <ToUserName><![CDATA[toUser]]></ToUserName>  <FromUserName><![CDATA[FromUser]]></FromUserName>  <CreateTime>123456789</CreateTime>  <MsgType><![CDATA[event]]></MsgType>  <Event><![CDATA[VIEW]]></Event>  <EventKey><![CDATA[www.qq.com]]></EventKey></xml>";
		defaultWxmpMessageHandle.processXmlData(xml);
	}

}
