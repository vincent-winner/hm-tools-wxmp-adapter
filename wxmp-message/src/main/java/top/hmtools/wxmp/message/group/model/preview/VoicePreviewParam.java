package top.hmtools.wxmp.message.group.model.preview;

public class VoicePreviewParam extends BasePreviewParam {

	private MediaId voice;

	public MediaId getVoice() {
		return voice;
	}

	public void setVoice(MediaId voice) {
		this.voice = voice;
	}

	@Override
	public String toString() {
		return "VoicePreviewParam [voice=" + voice + ", msgtype=" + msgtype + ", touser=" + touser + "]";
	}
}
