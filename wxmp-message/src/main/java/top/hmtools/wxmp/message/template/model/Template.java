package top.hmtools.wxmp.message.template.model;

/**
 * Auto-generated: 2019-08-29 11:22:9
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Template {

	private String template_id;
	private String title;
	private String primary_industry;
	private String deputy_industry;
	private String content;
	private String example;

	public void setTemplate_id(String template_id) {
		this.template_id = template_id;
	}

	public String getTemplate_id() {
		return template_id;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitle() {
		return title;
	}

	public void setPrimary_industry(String primary_industry) {
		this.primary_industry = primary_industry;
	}

	public String getPrimary_industry() {
		return primary_industry;
	}

	public void setDeputy_industry(String deputy_industry) {
		this.deputy_industry = deputy_industry;
	}

	public String getDeputy_industry() {
		return deputy_industry;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getContent() {
		return content;
	}

	public void setExample(String example) {
		this.example = example;
	}

	public String getExample() {
		return example;
	}

}