package top.hmtools.wxmp.message.group.model;

public class GroupMessageSendStatusParam {

	private String msg_id;

	public String getMsg_id() {
		return msg_id;
	}

	public void setMsg_id(String msg_id) {
		this.msg_id = msg_id;
	}

	@Override
	public String toString() {
		return "GroupMessageSendStatusParam [msg_id=" + msg_id + "]";
	}
	
	
}
