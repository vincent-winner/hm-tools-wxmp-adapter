package top.hmtools.wxmp.message.group.model.preview;

import top.hmtools.wxmp.core.model.ErrcodeBean;

/**
 * Auto-generated: 2019-08-26 10:21:47
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class PreviewResult extends ErrcodeBean {

	/**
	 * 消息发送任务的ID
	 */
	private int msg_id;

	public int getMsg_id() {
		return msg_id;
	}

	public void setMsg_id(int msg_id) {
		this.msg_id = msg_id;
	}

	@Override
	public String toString() {
		return "PreviewResult [msg_id=" + msg_id + ", errcode=" + errcode + ", errmsg=" + errmsg + "]";
	}
	


}