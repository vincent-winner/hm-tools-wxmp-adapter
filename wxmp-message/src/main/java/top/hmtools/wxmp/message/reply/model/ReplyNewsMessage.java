package top.hmtools.wxmp.message.reply.model;

import java.util.List;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import top.hmtools.wxmp.core.model.message.BaseMessage;

/**
 * 回复图文消息
 * {@code
 * <xml>
  <ToUserName><![CDATA[toUser]]></ToUserName>
  <FromUserName><![CDATA[fromUser]]></FromUserName>
  <CreateTime>12345678</CreateTime>
  <MsgType><![CDATA[news]]></MsgType>
  <ArticleCount>1</ArticleCount>
  <Articles>
    <item>
      <Title><![CDATA[title1]]></Title>
      <Description><![CDATA[description1]]></Description>
      <PicUrl><![CDATA[picurl]]></PicUrl>
      <Url><![CDATA[url]]></Url>
    </item>
  </Articles>
</xml>
 * }
 * @author hybo
 *
 */
public class ReplyNewsMessage extends BaseMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7854301878376985670L;

	/**
	 * 图文消息个数；当用户发送文本、图片、视频、图文、地理位置这五种消息时，开发者只能回复1条图文消息；其余场景最多可回复8条图文消息
	 */
	@XStreamAlias("ArticleCount")
	private int articleCount;
	
	/**
	 * 图文消息信息，注意，如果图文数超过限制，则将只发限制内的条数
	 */
	private List<ReplyArticle> Articles;

	public int getArticleCount() {
		if(this.Articles!=null){
			return this.Articles.size();
		}else{
			return -1;
		}
	}

	public void setArticleCount(int articleCount) {
		this.articleCount = articleCount;
	}

	public List<ReplyArticle> getArticles() {
		return Articles;
	}

	public void setArticles(List<ReplyArticle> articles) {
		Articles = articles;
		if(this.Articles != null){
			this.articleCount = this.Articles.size();
		}
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public void configXStream(XStream xStream) {
		
	}

	
	
}
