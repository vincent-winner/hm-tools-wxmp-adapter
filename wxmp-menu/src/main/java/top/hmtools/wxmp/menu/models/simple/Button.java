package top.hmtools.wxmp.menu.models.simple;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

public class Button implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6200574788146574103L;

	/**
	 * 菜单的响应动作类型，view表示网页类型，click表示点击类型，miniprogram表示小程序类型
	 */
	private String type;

	/**
	 * 菜单标题，不超过16个字节，子菜单不超过60个字节
	 */
	private String name;

	/**
	 * 菜单KEY值，用于消息接口推送，不超过128字节
	 */
	private String key;

	/**
	 * 二级菜单数组，个数应为1~5个
	 */
	private List<Button> sub_button;

	/**
	 * 网页 链接，用户点击菜单可打开链接，不超过1024字节。 type为miniprogram时，不支持小程序的老版本客户端将打开本url。
	 */
	private String url;

	/**
	 * 小程序的appid（仅认证公众号可配置）
	 */
	private String appid;

	/**
	 * 小程序的页面路径
	 */
	private String pagepath;

	/**
	 * 调用新增永久素材接口返回的合法media_id
	 */
	private String media_id;

	/**
	 * 菜单的响应动作类型，view表示网页类型，click表示点击类型，miniprogram表示小程序类型
	 * 
	 * @return
	 */
	public String getType() {
		return type;
	}

	/**
	 * 菜单的响应动作类型，view表示网页类型，click表示点击类型，miniprogram表示小程序类型
	 * 
	 * @param type
	 */
	public Button setType(String type) {
		this.type = type;
		return this;
	}

	/**
	 * 菜单标题，不超过16个字节，子菜单不超过60个字节
	 * 
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * 菜单标题，不超过16个字节，子菜单不超过60个字节
	 * 
	 * @param name
	 */
	public Button setName(String name) {
		this.name = name;
		return this;
	}

	/**
	 * 菜单KEY值，用于消息接口推送，不超过128字节
	 * 
	 * @return
	 */
	public String getKey() {
		return key;
	}

	/**
	 * 菜单KEY值，用于消息接口推送，不超过128字节
	 * 
	 * @param key
	 */
	public Button setKey(String key) {
		this.key = key;
		return this;
	}

	/**
	 * 二级菜单数组，个数应为1~5个
	 * 
	 * @return
	 */
	public List<Button> getSub_button() {
		return sub_button;
	}

	/**
	 * 二级菜单数组，个数应为1~5个
	 * 
	 * @param sub_button
	 */
	public Button setSub_button(List<Button> sub_button) {
		if (sub_button.size() > 5) {
			this.sub_button.addAll(sub_button.subList(0, 5));
		} else {
			this.sub_button = sub_button;
		}
		return this;
	}
	
	/**
	 * 添加二级菜单数组，个数应为1~5个
	 * @param buttonBeans
	 * @return
	 */
	public Button addSubButton(Button...buttonBeans){
		return this.setSub_button(Arrays.asList(buttonBeans));
	}

	/**
	 * 网页 链接，用户点击菜单可打开链接，不超过1024字节。 type为miniprogram时，不支持小程序的老版本客户端将打开本url。
	 * 
	 * @return
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * 网页 链接，用户点击菜单可打开链接，不超过1024字节。 type为miniprogram时，不支持小程序的老版本客户端将打开本url。
	 * 
	 * @param url
	 */
	public Button setUrl(String url) {
		this.url = url;
		return this;
	}

	/**
	 * 小程序的appid（仅认证公众号可配置）
	 * 
	 * @return
	 */
	public String getAppid() {
		return appid;
	}

	/**
	 * 小程序的appid（仅认证公众号可配置）
	 * 
	 * @param appid
	 */
	public Button setAppid(String appid) {
		this.appid = appid;
		return this;
	}

	/**
	 * 小程序的页面路径
	 * 
	 * @return
	 */
	public String getPagepath() {
		return pagepath;
	}

	/**
	 * 小程序的页面路径
	 * 
	 * @param pagepath
	 */
	public Button setPagepath(String pagepath) {
		this.pagepath = pagepath;
		return this;
	}

	/**
	 * 调用新增永久素材接口返回的合法media_id
	 * 
	 * @return
	 */
	public String getMedia_id() {
		return media_id;
	}

	/**
	 * 调用新增永久素材接口返回的合法media_id
	 * 
	 * @param media_id
	 */
	public Button setMedia_id(String media_id) {
		this.media_id = media_id;
		return this;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "ButtonBean [type=" + type + ", name=" + name + ", key=" + key + ", sub_button=" + sub_button + ", url="
				+ url + ", appid=" + appid + ", pagepath=" + pagepath + ", media_id=" + media_id + "]";
	}

}
