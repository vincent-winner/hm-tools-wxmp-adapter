package top.hmtools.wxmp.account.models.eventMessage;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import top.hmtools.wxmp.core.annotation.WxmpMessage;
import top.hmtools.wxmp.core.model.message.BaseEventMessage;
import top.hmtools.wxmp.core.model.message.enums.Event;
import top.hmtools.wxmp.core.model.message.enums.MsgType;

/**
 * 名称认证失败（这时虽然客户端不打勾，但仍有接口权限）
 * {@code
 * <xml>
  <ToUserName><![CDATA[toUser]]></ToUserName>  
  <FromUserName><![CDATA[fromUser]]></FromUserName>  
  <CreateTime>1442401061</CreateTime>  
  <MsgType><![CDATA[event]]></MsgType>  
  <Event><![CDATA[naming_verify_fail]]></Event>  
  <FailTime>1442401061</FailTime>  
  <FailReason><![CDATA[by time]]></FailReason> 
</xml>
 * }
 * @author HyboWork
 *
 */
@WxmpMessage(msgType=MsgType.event,event=Event.naming_verify_fail)
public class NamingVerifyFailMessage extends BaseEventMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7345884077805046679L;
	
	@XStreamAlias("FailTime")
	private Long failTime;
	
	@XStreamAlias("FailReason")
	private String failReason;
	
	

	public Long getFailTime() {
		return failTime;
	}



	public void setFailTime(Long failTime) {
		this.failTime = failTime;
	}



	public String getFailReason() {
		return failReason;
	}



	public void setFailReason(String failReason) {
		this.failReason = failReason;
	}



	public static long getSerialversionuid() {
		return serialVersionUID;
	}



	@Override
	public String toString() {
		return "NamingVerifyFail [failTime=" + failTime + ", failReason=" + failReason + ", event=" + event
				+ ", eventKey=" + eventKey + ", toUserName=" + toUserName + ", fromUserName=" + fromUserName
				+ ", createTime=" + createTime + ", msgType=" + msgType + ", msgId=" + msgId + "]";
	}



	@Override
	public void configXStream(XStream xStream) {

	}

}
