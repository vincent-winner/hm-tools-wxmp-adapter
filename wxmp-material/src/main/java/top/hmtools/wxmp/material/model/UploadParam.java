package top.hmtools.wxmp.material.model;

import java.io.File;

import top.hmtools.wxmp.material.enums.MediaType;

public class UploadParam {

	private MediaType type;
	
	private File media;
	
	/**
	 * 说明信息，当上传永久视频素材时需要使用，此时须 {@link top.hmtools.wxmp.material.model.DescriptionBean}设置好对应字段值，再转换成json字符串，并复制本属性
	 */
	private String description;

	public MediaType getType() {
		return type;
	}

	public void setType(MediaType type) {
		this.type = type;
	}

	public File getMedia() {
		return media;
	}

	public void setMedia(File media) {
		this.media = media;
	}

	public String getDescription() {
		return description;
	}

	/**
	 * 说明信息，当上传永久视频素材时需要使用，此时须 {@link top.hmtools.wxmp.material.model.DescriptionBean}设置好对应字段值，再转换成json字符串，并复制本属性
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "UploadParam [type=" + type + ", media=" + media + ", description=" + description + "]";
	}

	
}
